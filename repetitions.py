import os
#from sklearn.model_selection import ShuffleSplit
from shutil import copyfile 
import numpy as np
import cv2
import shutil
import random
import xml.etree.ElementTree as ET

class Repetition:
	def __init__(self, path_out, path, num_images_train, validation):		
		self.path = path
		self.path_out = path_out
		self.num_images_train = num_images_train
		self.validation = validation

	def get_path(self):
		return self.path

	def get_path_out(self):
		return self.path_out

	def listdir_nohidden(self,path):
		folders = []
		for f in os.listdir(path):
			content = path +'/'+f
			if os.path.isdir(content):
				if not f.startswith('.'):
					folders.append(f)
		return folders

	def __listDir(self, path):
		if os.path.exists(path):
			return self.listdir_nohidden(path)
		else:
			return None

	def __listImages(self, path):
		files_imgs = []
		files = os.listdir(path)
		for file in files:
			name, extension = os.path.splitext(file)
			if extension.lower() == '.jpg'.lower() or extension.lower() == '.png'.lower():
				file_name = path  + file
				files_imgs.append(file_name)
		return files_imgs

	def createDirectory(self, directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

	def readImage(self, path):
		'''
		Read image of the path
		:param path: path to image
		:return: image
		'''
		img = cv2.imread(path)
		return img

	def read_dataset(self):
		'''
		Retorna um dictionary onde key é a classe e o value é uma lista com os nomes de aruivos de imagens
		exemplo: {
					'soja_doente': ['img1.jpg','img2.jpg','img3.jpg'],
					'soja_sadia': ['img4.jpg','img5.jpg','img6.jpg']
				 }
		:param path: path to image
		:return: image
		'''		
		images_dict = {}
		list_class = self.__listDir(self.get_path()) # lista todas as pastas, classes

		if list_class is not None:
			for cl in list_class: 
				list_images = []				
				images = self.__listImages(self.get_path() + '/' + cl + '/')
				for img in images: # para cada classe, pega todos os nomes de arquivo de imagens
					list_images.append(img) # adiciona cada nome de arquivo de imagem em uma lista
				images_dict[cl] = list_images # key -> o nome da classe, value -> a lista com o nomes dos arquivos da classe
		return images_dict

	def copy_files(self, src, dst):
		name, _ = os.path.splitext(src)
		src_xml = name + '.xml'		
		src_txt = name + '.txt'
		name, _ = os.path.splitext(dst)
		path_img = name + '.jpg'
		dst_xml = name + '.xml'
		dst_txt = name + '.txt'
		copyfile(src_xml, dst_xml)
		if os.path.exists(src_txt):
			copyfile(src_txt, dst_txt)
		self.updadePathXml(dst_xml, path_img)

	def getFolderName(self):
		path_out = self.path_out
		out = path_out.split('/')
		out_name = out[len(out)-1]
		return out_name

	def defineRepetitions(self):
		'''
		separa as imagens em repetições para treinamento, validação e teste
		'''
		percentage_val = .1
		images_dict = self.read_dataset()
		for class_, images in images_dict.items():
			for idx in range(10): # 10 repetições para cada classe
				random.shuffle(images)
				images_train = images[0:self.num_images_train]
				index_images_test = self.num_images_train + 70
				images_test = images[self.num_images_train:index_images_test]

				directory_train = self.path_out + '/' + self.getFolderName() + str(idx+1) + '/' + 'train/' + class_ + '/'
				directory_test = self.path_out + '/' + self.getFolderName() + str(idx+1) + '/' + 'test/' + class_ + '/'
				self.createDirectory(directory_train)
				self.createDirectory(directory_test)

				if self.validation:
					directory_validation = self.path_out + '/' + self.getFolderName() + str(idx+1) + '/' + 'validation/' + class_ + '/'
					self.createDirectory(directory_validation)
					percentage_validation = len(images_train) * (percentage_val * 100) / 100
					num_images_val = round(percentage_validation)
					if num_images_val == 0: num_images_val = 1
					images_validation = images_train[0:num_images_val]					
					images_train = images_train[num_images_val: len(images_train)] #remove as imagens para validação

					for path in images_validation:
						img = self.readImage(path)
						_, file = os.path.split(path)
						path_img = directory_validation + file
						cv2.imwrite(path_img ,img)
						self.copy_files(path, path_img)

				for path in images_train:								
					img = self.readImage(path)
					_, file = os.path.split(path)
					path_img = directory_train + file
					cv2.imwrite(path_img ,img)
					self.copy_files(path, path_img)

				for path in images_test:								
					img = self.readImage(path)
					_, file = os.path.split(path)
					path_img = directory_test + file
					cv2.imwrite(path_img ,img)
					self.copy_files(path, path_img)

	def updadePathXml(self, file, text):
		tree = ET.parse(file)
		tree.find("path").text = text
		tree.write(file)
	
	def sampling(self):
		if not os.path.exists(self.get_path_out()):
			print('Não existe repetições, contruindo repetições...')
			self.defineRepetitions()
		else:
			print('Folds já existem.')
			exit() # significa que já foi realizado o augmentation e oversampling

if __name__ == '__main__':
	path_in = './data/annotated_pests_v2'
	path_out = './data/repetition'
	num_images_train = 5
	validation = True
	cv = Repetition(path_out, path_in, num_images_train, validation)
	cv.sampling()
