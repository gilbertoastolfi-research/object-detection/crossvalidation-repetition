import random
import os
import operator
import xml.etree.ElementTree as et
import cv2
from transformation_image import Transformation
import argparse

class DataAugmentation:
	def __init__(self, path_folds, percentage):
		self.root_path = path_folds
		self.percentage = percentage
		self.transformation = Transformation()
		self.idx_continue = 0

	def readImage(self, xml_file ):
		root = et.parse(xml_file).getroot()
		image_path = root.find("path").text		
		image = cv2.imread(image_path, cv2.IMREAD_COLOR)		
		try:
			image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		except:
			name, extension = os.path.splitext(image_path)
			if extension == '.jpg': extension = '.JPG'
			else: extension = '.jpg'
			image_path = name + extension
			image = cv2.imread(image_path, cv2.IMREAD_COLOR)
			image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)				
		return image

	def pathImageFromXml(self, xml_file):
		root = et.parse(xml_file).getroot()
		image_path = root.find("path").text
		return image_path

	def listXmlFromClass(self, path):
		files_imgs = []
		files = os.listdir(path)
		for file in files:
			name, extension = os.path.splitext(file)
			if extension.lower() == '.xml'.lower():
				file_name = path + '/' + file
				name_splited = file_name.split('/')
				name_image = name_splited[len(name_splited)-1]
				if '_aug_' not in name_image and '_over_' not in name_image:
					files_imgs.append(file_name)
		return files_imgs

	def listBoundingBoxImage(self, xml_file):
		root = et.parse(xml_file).getroot()		
		bndbox = []
		for obj in root.findall('object'):
			class_name = obj.find('name').text
			xmin = int(obj.find('bndbox').find('xmin').text)
			ymin = int(obj.find('bndbox').find('ymin').text)
			xmax = int(obj.find('bndbox').find('xmax').text)
			ymax = int(obj.find('bndbox').find('ymax').text)
			bndbox.append([xmin,ymin,xmax,ymax,class_name])
		return bndbox

	def createFileXml(self, filename, size, classe, bndbox, path_xml, path):
		out_xml = '<annotation>\n'
		out_xml += '\t<folder>img</folder>\n'
		out_xml += '\t<filename>'+filename+'</filename>\n'
		out_xml += '\t<path>'+path+'</path>\n'
		out_xml += '\t<source>\n'
		out_xml += '\t\t<database>Unknown</database>\n'
		out_xml += '\t</source>\n'
		out_xml += '\t<size>\n'
		out_xml += '\t\t<width>'+str(size[1])+'</width>\n'
		out_xml += '\t\t<height>'+str(size[0])+'</height>\n'
		out_xml += '\t\t<depth>'+str(size[2])+'</depth>\n'
		out_xml += '\t</size>\n'
		out_xml += '\t<segmented>0</segmented>\n'
		out_xml += '\t<object>\n'
		out_xml += '\t\t<name>'+classe+'</name>\n'
		out_xml += '\t\t<pose>Unspecified</pose>\n'
		out_xml += '\t\t<truncated>0</truncated>\n'
		out_xml += '\t\t<difficult>0</difficult>\n'
		out_xml += '\t\t<bndbox>\n'
		out_xml += '\t\t\t<xmin>'+str(bndbox[0])+'</xmin>\n'
		out_xml += '\t\t\t<ymin>'+str(bndbox[1])+'</ymin>\n'
		out_xml += '\t\t\t<xmax>'+str(bndbox[2])+'</xmax>\n'
		out_xml += '\t\t\t<ymax>'+str(bndbox[3])+'</ymax>\n'
		out_xml += '\t\t</bndbox>\n'
		out_xml += '\t</object>\n'
		out_xml += '</annotation>'
		fo = open(path_xml, "w")
		fo.write(out_xml)
		fo.close()

	def saveImageAnnotation(self, rotated_images, src, label):

		file_name, _ = os.path.splitext(src)# path do arquivo original sem extensão. Exemplo: path/path/inseto1

		num_images = 1
		for ri in rotated_images: #(cl, rotated_image, new_boundingbox)
			path_image = file_name + label +str(num_images)+'.jpg' # tag path e para salvar o arquivo jpg
			path_xml = file_name + label +str(num_images)+'.xml' # para salvar o arquivo xml
			
			if os.path.isfile(path_xml):
				print ("-----------------> File exist")

			name_splited = path_image.split('/')
			filename = name_splited[len(name_splited)-1] #tag filename. Exemplo: inseto1.jpg
			size = ri[1].shape
			classe = ri[0]
			bndbox = ri[2]
			self.createFileXml(filename, size, classe, bndbox, path_xml, path_image)

			image = ri[1]
			img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
			cv2.imwrite(path_image, img)
			num_images = num_images + 1


	def applyAugmentationInDir(self, list_xml, num_image):
		for idx in range(num_image):
			#idx = random.randint(0, len(list_xml)-1) # get a xml file randomly
			xml = list_xml[idx]
			original_image = self.readImage(xml)
			list_bb = self.listBoundingBoxImage(xml)
			path_image = self.pathImageFromXml(xml)
			trans_images = [] # conjunto de imagens geradas para cada bb do xml
			# cria uma imagem para cada bb
			for bb_ in list_bb:
				bb = bb_[0:4]
				cl = bb_[4]
				rotated_images = self.transformation.applyTransformation(original_image, bb, "rotation", 1)
				rotated_image = rotated_images[0][0]
				new_boundingbox = rotated_images[0][1]
				trans_images.append((cl, rotated_image, new_boundingbox))
			
			self.saveImageAnnotation(trans_images, path_image, '_aug_')

	def applyAugmentationInSet(self, path, classes):
		for cl in classes:
			path_class = path + '/' + cl				
			list_xml = self.listXmlFromClass(path_class)
			num_image = int(len(list_xml) * self.percentage) # Calculates the number of images to be generated 
			self.applyAugmentationInDir(list_xml, num_image)

	def augmentation(self):
		print('Iniciando aumento de dados...')
		folds = os.listdir(self.root_path)
		for fold in folds:
			path_train = self.root_path + '/' + fold + '/train'
			path_validation = self.root_path + '/' + fold + '/validation'
			classes_train = os.listdir(path_train)
			classes_validation = os.listdir(path_validation)
			self.applyAugmentationInSet(path_train, classes_train)
			self.applyAugmentationInSet(path_validation, classes_validation)
		print('Fim aumento de dados...')

	def countClass(self, xml_file):
		root = et.parse(xml_file).getroot()		
		num_classes = {}
		for obj in root.findall('object'):
			class_name = obj.find('name').text
			if class_name in num_classes:
				num_classes[class_name] +=1
			else:
				num_classes[class_name] =1
		return num_classes

	def countClassDir(self, path):
		num_classes = {}
		classes = os.listdir(path)
		for cl in classes:
			path_class = path + '/' + cl
			files = os.listdir(path_class)
			for file in files:
				name, extension = os.path.splitext(file)
				if extension.lower() == '.xml'.lower():
					file_name = path_class + '/' + file
					n_classes = self.countClass(file_name)
					for class_name, number in n_classes.items():
						if class_name in num_classes:
							n = num_classes.get(class_name)
							num_classes[class_name] = (number + n)
						else:
							num_classes[class_name] = number
		return num_classes # exemplo -> {'edessa_body': 39, 'edessa_head': 39, 'edessa_paw': 204}

	#verifica se a classe está no xml
	def hasClass(self, xml_file, classe):
		root = et.parse(xml_file).getroot()	
		for obj in root.findall('object'):
			class_name = obj.find('name').text
			if class_name == classe:
				return True
		return False

	#lista todos os xml que tem a classe em questão
	def listAllXmlFromClass(self, path, classe):
		xml_with_class = []
		classes = os.listdir(path)
		for cl in classes:
			path_class = path + '/' + cl
			files = self.listXmlFromClass(path_class)
			for file in files:
				if self.hasClass(file, classe):
					xml_with_class.append(file)
		return xml_with_class

	def listBoundingBoxFromClassInImage(self, xml_file, classe):
		root = et.parse(xml_file).getroot()		
		bndbox = []
		for obj in root.findall('object'):
			class_name = obj.find('name').text
			if class_name == classe:
				xmin = int(obj.find('bndbox').find('xmin').text)
				ymin = int(obj.find('bndbox').find('ymin').text)
				xmax = int(obj.find('bndbox').find('xmax').text)
				ymax = int(obj.find('bndbox').find('ymax').text)
				bndbox.append([xmin,ymin,xmax,ymax,class_name])
		return bndbox
				
	# path: train ou validation
	# quant: número de anotações da classe
	def applyOversamplingInClass(self, path, num_gerar, classe):
		list_xml = self.listAllXmlFromClass(path, classe) # lista todos os xml que tem pelo menos uma anotação da classe
		for over in range(num_gerar):
			idx = random.randint(0, len(list_xml)-1) #gera um indice aleatóriamente
			xml = list_xml[idx] # pega um xml aleatóriamente
			original_image = self.readImage(xml)
			#lista somente os bb da classe
			list_bb_from_class = self.listBoundingBoxFromClassInImage(xml, classe)
			idxbb = 0 
			if len(list_bb_from_class) > 1:
				#gera um indice aleatóriamente
				idxbb = random.randint(0, len(list_bb_from_class)-1) # 
			bb_ = list_bb_from_class[idxbb] #pega um bb aleatóriamente da lista
			bb = bb_[0:4] # bb
			cl = bb_[4] # classe
			
			rotated_images = self.transformation.applyTransformation(original_image, bb, "rotation", 1)
			rotated_image = rotated_images[0][0]
			new_boundingbox = rotated_images[0][1]
			trans_images = [] # conjunto de imagens geradas para cada bb do xml
			trans_images.append((cl, rotated_image, new_boundingbox))
			path_image = self.pathImageFromXml(xml)
			label = '_over_'+str(self.idx_continue)+'_'
			self.idx_continue += 1
			self.saveImageAnnotation(trans_images, path_image,label)



	def applyOversamplingInSet(self, path, classes):
		num_classes = self.countClassDir(path)
		max_value = max(num_classes.values()) # indica a classe que tem maior número de anotações
		#percorre as classes e obtem o numero de anotações que cada uma passui
		for cl, num_anot in num_classes.items():
			num_gerar = max_value - num_anot # número de imagens a serem geradas
			#print(cl,' ---> ',num_gerar, ' -- ', num_gerar+num_anot)
			if num_gerar == 0:
				continue
			else:
				# gerar num_gerar de imagens para a classe cl
				self.applyOversamplingInClass(path, num_gerar, cl)
		
	def oversampling(self):
		print('Iniciando oversampling...')
		folds = os.listdir(self.root_path)
		for fold in folds:
			path_train = self.root_path + '/' + fold + '/train'
			path_validation = self.root_path + '/' + fold + '/validation'
			classes_train = os.listdir(path_train)
			classes_validation = os.listdir(path_validation)
			#print(10*'**',fold)
			#print(self.countClassDir(path_train))
			self.applyOversamplingInSet(path_train, classes_train)
			#print(self.countClassDir(path_train))
			#print(15*'--')
			#print(self.countClassDir(path_validation))
			self.applyOversamplingInSet(path_validation, classes_validation)
			#print(self.countClassDir(path_validation))
		print('Fim oversampling...')


if __name__ == '__main__':
	path_out = './data/folds'
	dt = DataAugmentation(path_out, .3)
	dt.augmentation()
	dt.oversampling()