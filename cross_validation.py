import os
from sklearn.model_selection import ShuffleSplit
from shutil import copyfile 
import numpy as np
import cv2
import shutil
import xml.etree.ElementTree as ET

class CrossValidation:
	def __init__(self, path_out, path, number_fold, validation):		
		self.path = path
		self.path_out = path_out
		self.number_fold = number_fold
		self.validation = validation

	def get_path(self):
		return self.path

	def get_path_out(self):
		return self.path_out

	def listdir_nohidden(self,path):
		folders = []
		for f in os.listdir(path):
			content = path +'/'+f
			if os.path.isdir(content):
				if not f.startswith('.'):
					folders.append(f)
		return folders

	def __listDir(self, path):
		if os.path.exists(path):
			return self.listdir_nohidden(path)
		else:
			return None

	def __listImages(self, path):
		files_imgs = []
		files = os.listdir(path)
		for file in files:
			name, extension = os.path.splitext(file)
			if extension.lower() == '.jpg'.lower() or extension.lower() == '.png'.lower():
				file_name = path  + file
				files_imgs.append(file_name)
		return files_imgs

	def createDirectory(self, directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

	def readImage(self, path):
		'''
		Read image of the path
		:param path: path to image
		:return: image
		'''
		img = cv2.imread(path)
		return img

	def read_dataset(self):
		'''
		Retorna um dictionary onde key é a classe e o value é uma lista com os nomes de aruivos de imagens
		exemplo: {
					'soja_doente': ['img1.jpg','img2.jpg','img3.jpg'],
					'soja_sadia': ['img4.jpg','img5.jpg','img6.jpg']
				 }
		:param path: path to image
		:return: image
		'''		
		images_dict = {}
		list_class = self.__listDir(self.get_path()) # lista todas as pastas, classes

		if list_class is not None:
			for cl in list_class: 
				list_images = []				
				images = self.__listImages(self.get_path() + '/' + cl + '/')				
				for img in images: # para cada classe, pega todos os nomes de arquivo de imagens
					list_images.append(img) # adiciona cada nome de arquivo de imagem em uma lista
				images_dict[cl] = list_images # key -> o nome da classe, value -> a lista com o nomes dos arquivos da classe
		return images_dict

	def copy_files(self, src, dst):
		name, _ = os.path.splitext(src)
		src_xml = name + '.xml'		
		src_txt = name + '.txt'
		name, _ = os.path.splitext(dst)
		path_img = name + '.jpg'
		dst_xml = name + '.xml'
		dst_txt = name + '.txt'
		copyfile(src_xml, dst_xml)
		copyfile(src_txt, dst_txt)
		self.updadePathXml(dst_xml, path_img)

	def getFolderName(self):
		path_out = self.path_out
		out = path_out.split('/')
		out_name = out[len(out)-1]
		return out_name

	def defineFolds(self):
		'''
		separa as imagens em dobras para treinamento e teste
		'''
		percentage_test = 0
		if self.number_fold == 5:
			percentage_test = .2
		elif self.number_fold == 10:
			percentage_test = .1
		else:
			print('Informe o número de dobras.')

		cv = ShuffleSplit(n_splits=self.number_fold, test_size=percentage_test, random_state=0)
		
		images_dict = self.read_dataset()
		for class_, images in images_dict.items():
			cv.get_n_splits(images)
			number_fold = 1
			validation_index = None
			img_train, img_validation, img_test = None, None, None
			for train_index, test_index in cv.split(images):
				directory_train = self.path_out + '/' + self.getFolderName() + str(number_fold) + '/' + 'train/' + class_ + '/'
				directory_test = self.path_out + '/' + self.getFolderName() + str(number_fold) + '/' + 'test/' + class_ + '/'
				self.createDirectory(directory_train)
				self.createDirectory(directory_test)

				if self.validation:
					directory_validation = self.path_out + '/' + self.getFolderName() + str(number_fold) + '/' + 'validation/' + class_ + '/'
					self.createDirectory(directory_validation)
					percentage_validation = len(images) * (percentage_test * 100) / 100
					validation_index = train_index[0:round(percentage_validation)]
					train_index = train_index[round(percentage_validation): len(train_index)]
					img_validation = [images[i] for i in validation_index]
					for path in img_validation:
						img = self.readImage(path)
						_, file = os.path.split(path)
						path_img = directory_validation + file
						cv2.imwrite(path_img ,img)
						self.copy_files(path, path_img)

				number_fold = number_fold + 1
				img_train = [images[i] for i in train_index] 
				img_test = [images[i] for i in test_index]
				for path in img_train:								
					img = self.readImage(path)
					_, file = os.path.split(path)
					path_img = directory_train + file
					cv2.imwrite(path_img ,img)
					self.copy_files(path, path_img)

				for path in img_test:								
					img = self.readImage(path)
					_, file = os.path.split(path)
					path_img = directory_test + file
					cv2.imwrite(path_img ,img)
					self.copy_files(path, path_img)

	def updadePathXml(self, file, text):
		tree = ET.parse(file)
		tree.find("path").text = text
		tree.write(file)
	
	def sampling(self):
		if not os.path.exists(self.get_path_out()):
			print('Não existe folds, contruindo folds...')
			self.defineFolds()
		else:
			print('Folds já existem.')
			exit() # significa que já foi realizado o augmentation e oversampling

if __name__ == '__main__':
	path_in = './data/annotations'
	path_out = './data/folds'
	number_fold = 5
	validation = True
	cv = CrossValidation(path_out, path_in, number_fold, validation)
	cv.sampling()
