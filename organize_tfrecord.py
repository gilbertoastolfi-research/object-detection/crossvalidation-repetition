import os
from shutil import copyfile
import xml.etree.ElementTree as et
import argparse

class Organize:
	def __init__(self, path_in, path_out):		
		self.path_in = path_in
		self.path_out = path_out

	def createDirectory(self, directory):
		if not os.path.exists(directory):
			os.makedirs(directory)

	def getFoldAndSetFromPath(self, path):
		splited = path.split('/')
		set_ = splited[len(splited)-1]
		fold = splited[len(splited)-2]
		return fold, set_

	def getXmlAndJpgFromSet(self, path):
		files_xml = []
		files_jpg = []
		files = os.listdir(path)
		for file in files:
			name, extension = os.path.splitext(file)
			if extension.lower() == '.xml'.lower():
				file_name = path + '/' + file
				files_xml.append(file_name)
			elif extension.lower() == '.jpg'.lower():
				file_name = path + '/' + file
				files_jpg.append(file_name)
		return files_xml, files_jpg

	def updadePathXml(self, file, text):
		tree = et.parse(file)
		tree.find("path").text = text
		tree.write(file)

	def transfer(self, list_files, to):
		for file in list_files:
			name_splited = file.split('/')
			name = name_splited[len(name_splited)-1]
			src = file
			dst = to + '/' + name			
			copyfile(src, dst)
			nam, extension = os.path.splitext(dst)
			if extension.lower() == '.xml'.lower(): 
				path = nam + '.jpg'
				path_img = path.replace("Annotations", "JPEGImages")
				self.updadePathXml(dst, path_img)

	def applyOrganizeInSet(self, path):
		fold, set_ = self.getFoldAndSetFromPath(path)
		path_out_set_ann = self.path_out + '/' + fold + '/' + set_ + '/Annotations'
		path_out_set_jpg = self.path_out + '/' + fold + '/' + set_ + '/JPEGImages'
		self.createDirectory(path_out_set_ann)
		self.createDirectory(path_out_set_jpg)
		classes_set = os.listdir(path)
		for cl in classes_set:
			path_class = path + '/' + cl
			files_xml, files_jpg = self.getXmlAndJpgFromSet(path_class)
			self.transfer(files_xml, path_out_set_ann)
			self.transfer(files_jpg, path_out_set_jpg)

	def listImagesDir(self, path):
		images = []
		for file in os.listdir(path):
			nam, extension = os.path.splitext(file)
			if extension.lower() == '.jpg'.lower():
				src = path +'/'+file
				images.append(src)
		return images
	'''
	def copyAllSet(self, src):
		fold, set_ = self.getFoldAndSetFromPath(src)
		path_out_set = self.path_out + '/' + fold + '/' + set_
		self.createDirectory(path_out_set) # cria a pasta do conjunto de saída
		classes_set = os.listdir(src)
		for cl_src in classes_set:
			path_cl_src = src + '/' + cl_src # classe origem
			images = self.listImagesDir(path_cl_src) # pega todos as imagens de origem
			for src_img in images:				
				splited = src_img.split('/')
				name_jpg = splited[len(splited)-1] # pega o nome da imagens
				class_dir = path_out_set + '/' + cl_src
				self.createDirectory(class_dir)
				dst_img = class_dir + '/' + name_jpg				
				copyfile(src_img, dst_img)
	'''
	def copyAllSet(self, src):
		fold, set_ = self.getFoldAndSetFromPath(src)
		path_out_set = self.path_out + '/' + fold + '/' + set_
		self.createDirectory(path_out_set) # cria a pasta do conjunto de saída
		classes_set = os.listdir(src)
		for cl_src in classes_set:
			path_cl_src = src + '/' + cl_src # classe origem
			images = self.listImagesDir(path_cl_src) # pega todos as imagens de origem
			for src_img in images:				
				splited = src_img.split('/')
				name_jpg = splited[len(splited)-1] # pega o nome da imagens
				#class_dir = path_out_set + '/' + cl_src
				#self.createDirectory(class_dir)
				dst_img = path_out_set + '/' + name_jpg				
				copyfile(src_img, dst_img)

	def organize(self):
		print('Iniciando organização...')
		folds = os.listdir(self.path_in)
		for fold in folds:
			path_train = self.path_in + '/' + fold + '/train'
			path_validation = self.path_in + '/' + fold + '/validation'
			path_test = self.path_in + '/' + fold + '/test'
			self.applyOrganizeInSet(path_train)
			self.applyOrganizeInSet(path_validation)
			self.copyAllSet(path_test)
		print('Fim organização...')

	def run(self):
		if not os.path.exists(self.path_out):			
			self.organize()
		else:
			print('Organização já existe.')
			exit() # significa que já foi realizado a organização

if __name__ == '__main__':
	# python3 organize-tfrecord.py ./data/folds ./data/tfrecord
	parser = argparse.ArgumentParser()
	parser.add_argument('path_in', metavar='path_in', type=str, help='A root directory where are repetitions or folds.')
	parser.add_argument('path_out', metavar='path_out', type=str, help='Path where the output will be created')
	
	args = parser.parse_args()
	path_in = args.path_in
	path_out = args.path_out
	
	dt = Organize(path_in, path_out)
	dt.run()