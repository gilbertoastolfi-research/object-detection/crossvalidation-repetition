import random
import numpy as np
import cv2

class Transformation:
	def __init__(self):
		self.t_dic = { "rotation": self.rotateImage}
		self.f_dic = { "rotation":(0, 90), "width_shift":(0, 0.5), "height_shift":(0, 0.5), "scale": (0.5, 1.5)}


	def positive(self, x):
		if x < 1: return 1
		else: return x

	'''
	Rotate image and compute new bounding box
	@param image - image to be rotated
	@param angle - rotation angle
	@param bounding_box - original bounding box
	@return: the rotated image and the new bounding box
	'''
	def rotateImage(self, image, angle, bounding_box ):	
		# get image dimension
		img_height, img_width = image.shape[:2]
		
		# get rotation matrix
		rotation_matrix = cv2.getRotationMatrix2D( center = (img_width // 2, img_height // 2), angle = angle, scale = 1.0 )
	   
		# apply transformation (ratate image) 
		rotated_image = cv2.warpAffine( image, rotation_matrix, (img_width, img_height) )
		
		# --- compute new bounding box ---
		# Apply same transformation to the four bounding box corners
		rotated_point_A = np.matmul( rotation_matrix, np.array( [bounding_box[0], bounding_box[1], 1] ).T )   
		rotated_point_B = np.matmul( rotation_matrix, np.array( [bounding_box[2], bounding_box[1], 1] ).T )   
		rotated_point_C = np.matmul( rotation_matrix, np.array( [bounding_box[2], bounding_box[3], 1] ).T )   
		rotated_point_D = np.matmul( rotation_matrix, np.array( [bounding_box[0], bounding_box[3], 1] ).T )   
		# Compute new bounding box, that is, the bounding box for rotated object
		x = np.array( [ rotated_point_A[0], rotated_point_B[0], rotated_point_C[0], rotated_point_D[0] ] )
		y = np.array( [ rotated_point_A[1], rotated_point_B[1], rotated_point_C[1], rotated_point_D[1] ] )
		#x = positive(np.min( x ).astype(int))
		new_boundingbox = [self.positive(np.min(x).astype(int)), self.positive(np.min( y ).astype(int)), 
							self.positive(np.max( x ).astype(int)), self.positive(np.max( y ).astype(int))]
		
		return rotated_image, new_boundingbox


	def applyTransformation(self, image, bounding_box, transformation, n ):	
		t_images_list = []	
		for i in range(0, n):
			interval = self.f_dic[transformation]
			factor = random.uniform(interval[0], interval[1])
			img, bb = self.t_dic[transformation]( image, factor, bounding_box )
			t_images_list.append((img, bb, factor))
		return t_images_list
