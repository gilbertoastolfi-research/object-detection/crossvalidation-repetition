
from cross_validation import CrossValidation
from repetitions import Repetition
from augmentation import DataAugmentation
import argparse

if __name__ == '__main__':

	# python3 main.py ./data/annotated_pests_v2 ./data/folds repetition 30 True .3 True
	parser = argparse.ArgumentParser()
	parser.add_argument('path_in', metavar='path_in', type=str, help='A root directory where are annotated images.')
	parser.add_argument('path_out', metavar='path_out', type=str, help='Path where the output will be created')
	parser.add_argument('type', metavar='type', default='cross', type=str, help='"cross" or "repetition".')
	parser.add_argument('number', default='30', metavar='number', type=str, help='Number of image in train for repetition or number de folds for cross-validation (5 or 10).')
	parser.add_argument('validation', default='True', metavar='validation', type=str, help='If has validation')
	parser.add_argument('augmentation', default='0', metavar='augmentation', type=str, help='Percentual value for data augmentation')
	parser.add_argument('oversampling', default='True', metavar='oversampling', type=str, help='If has oversampling')
	
	
	args = parser.parse_args()
	path_in = args.path_in
	path_out = args.path_out
	type_ = args.type
	number = int(args.number)
	augmentation_value = float(args.augmentation)

	validation = args.validation
	if validation == 'True': validation = True
	else: validation = False

	oversampling = args.oversampling
	if oversampling == 'True': oversampling = True
	else: oversampling = False

	dt = DataAugmentation(path_out, augmentation_value)	

	if type_ == 'cross':
		cv = CrossValidation(path_out, path_in, number, validation)
		cv.sampling()
		
		if augmentation_value > 0:
			dt.augmentation()
		if oversampling:
			dt.oversampling()
		
	elif type_ == 'repetition':
		rep = Repetition(path_out, path_in, number, validation)
		rep.sampling()
		
		if augmentation_value > 0:
			dt.augmentation()
		if oversampling:
			dt.oversampling()
		
	else:
		print('Error')