# crossvalidation-repetition
# Responsáveis:
Gilberto Astolfi (gilbertoastolfi@gmail.com)
# Resumo:
Constrói dois tipos de amostragem para treinar a Faster-RCNN: validação cruzada e repetições. O projeto foi feito para ser usado no Google Colab. Para usá-lo faça o upload do arquivo crossvalidation_augmentation_oversampling_for_object_detection.ipynb em seu Google Colab e siga as instruções contidas neste notebook.
