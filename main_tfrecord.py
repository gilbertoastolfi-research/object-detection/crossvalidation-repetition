from organize_tfrecord import Organize
import argparse
import os

def execute_scripts(path_driver, path_ann, path_jpg, path_csv, path_pbtxt, path_tfr):
	os.system('python3 ' + path_driver+ '/generate_csv.py xml ' + path_ann + ' ' + path_csv)
	os.system('python3 ' + path_driver+ '/generate_pbtxt.py csv ' + path_csv + ' ' + path_pbtxt)
	os.system('python3 ' + path_driver+ '/generate_tfrecord.py ' + path_csv + ' ' + path_pbtxt + ' ' + path_jpg + ' ' + path_tfr)
		
def create_path(path, fold, set_):
	name = fold +'_'+set_
	path_ann = path + '/Annotations'
	path_jpg = path + '/JPEGImages'
	path_csv = path + '/' + name + '.csv'
	path_pbtxt = path + '/' + name + '.pbtxt'
	path_tfr = path + '/' + name + '.tfrecord'
	return path_ann, path_jpg, path_csv, path_pbtxt, path_tfr

def run_tfrecord(path_utils, path_tfrecord):
	print('Criando arquivos tfrecords...')
	folds = os.listdir(path_tfrecord)
	for fold in folds:
		path_train = path_tfrecord + '/' + fold + '/train'
		path_ann, path_jpg, path_csv, path_pbtxt, path_tfr = create_path(path_train, fold, 'train')
		execute_scripts(path_utils, path_ann, path_jpg, path_csv, path_pbtxt, path_tfr)
		path_validation = path_tfrecord + '/' + fold + '/validation'
		path_ann, path_jpg, path_csv, path_pbtxt, path_tfr = create_path(path_validation, fold, 'validation')
		execute_scripts(path_utils, path_ann, path_jpg, path_csv, path_pbtxt, path_tfr)
	print('Criação de arquivos tfrecords finalizada') 

def run_organize(path_in, path_out):
	if not os.path.exists(path_out):
		print('Organizando...')
		dt = Organize(path_in, path_out)
		dt.run()
	else:
		print('Conjunto já esta organizado.')
		exit() # significa que já foi organizado	


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('path_in', metavar='path_in', type=str, help='Path for where the repetitions or folds were created.')
	parser.add_argument('path_tfrecord', metavar='path_tfrecord', type=str, help='Path for where the repetitions or folds will be organized.')
	parser.add_argument('path_utils', metavar='path_utils', type=str, help='Path for where the https://github.com/douglasrizzo/detection_util_scripts.git was cloned.')
	
	args = parser.parse_args()
	path_in = args.path_in
	path_tfrecord = args.path_tfrecord
	run_organize(path_in, path_tfrecord)
	path_utils = args.path_utils
	run_tfrecord(path_utils, path_tfrecord)